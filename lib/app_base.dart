import 'package:flutter/material.dart';
import 'package:mobile_banking/screens/home_screen.dart';

class AppBase extends StatelessWidget {
  const AppBase();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomeScreen(),
      /* 
      SplashScreen(
        imageName: 'assets/splash1.png',
        subtitle: "Mobile banking",
        text:
            "Mobile banking is a service provided by a bank or other financial institution that allows its customers to conduct",
      ),*/
      theme: ThemeData(
        primaryColor: const Color(0xff24346C),
        // fontFamily: GoogleFonts.roboto(),
      ),
    );
  }
}
