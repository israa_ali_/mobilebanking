import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobile_banking/Models/history_item.dart';
import 'package:mobile_banking/history_items_list.dart';
import 'package:mobile_banking/widgets/history_painter.dart';
import 'package:mobile_banking/widgets/history_tile.dart';

class HistroyScreen extends StatelessWidget {
  const HistroyScreen();

  @override
  Widget build(BuildContext context) {
    List<HistoryItem> historyItems = HistoryItemsList().historyItems;
    print(historyItems.length);

    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Stack(
      //  fit: StackFit.passthrough,

      //  fit: StackFit.loose,
      alignment: Alignment.center,
      children: <Widget>[
        SizedBox(
            height: height * .9,
            width: width,
            child: Image.asset(
              'assets/history.png',
              fit: BoxFit.fill,
            )),
        Positioned(
          left: width * .03,
          top: height * .04,
          child: Text("History",
              style: GoogleFonts.roboto(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 28,
                  letterSpacing: 1)),
        ),
        Positioned(
          top: height * .25,
          child: Container(
            height: height,
            width: width,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20), color: Colors.white),
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  Container(
                    height: height * .015,
                    width: width * .25,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.black12),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Today",
                        style: GoogleFonts.roboto(
                          fontSize: 20,
                          fontWeight: FontWeight.w800,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        "Statistic",
                        style: GoogleFonts.roboto(
                          fontSize: 20,
                          fontWeight: FontWeight.w800,
                          color: Colors.black38,
                        ),
                      )
                    ],
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: historyItems.length,
                      itemBuilder: (BuildContext context, int index) {
                        //     print(historyItems[3].bill);
                        return HistoryTile(historyItems[index]);
                      },
                      scrollDirection: Axis.vertical,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),

        /* DraggableScrollableSheet(
                  builder: (context, ScrollController scrollController) =>
                      SingleChildScrollView(
                    controller: scrollController,
                    child: Container(
                      height: height * .8,
                      width: width * .8,
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              height: height * .015,
                              width: width * .25,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: Colors.black12),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "Today",
                                  style: GoogleFonts.roboto(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w800,
                                    color: Colors.black,
                                  ),
                                ),
                                Text(
                                  "Statistic",
                                  style: GoogleFonts.roboto(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w800,
                                    color: Colors.black38,
                                  ),
                                )
                              ],
                            ),
                            // ListView.builder(
                            //   itemCount: historyItems.length,
                            //   itemBuilder:
                            //       (BuildContext context, int index) {
                            //     print(historyItems[3].bill);
                            //     return HistoryTile(historyItems[index]);
                            //   },
                            //   scrollDirection: Axis.vertical,
                            // ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),*/
      ],
    );
  }
}
