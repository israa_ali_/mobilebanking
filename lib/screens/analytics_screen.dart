import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobile_banking/Models/employee.dart';
import 'package:mobile_banking/utils/get_data.dart';
import 'package:mobile_banking/widgets/analytics_painter.dart';
import 'package:mobile_banking/widgets/wallet_card.dart';

class AnalyticsScreen extends StatefulWidget {
  const AnalyticsScreen();

  @override
  _AnalyticsScreenState createState() => _AnalyticsScreenState();
}

class _AnalyticsScreenState extends State<AnalyticsScreen> {
  @override
  Widget build(BuildContext context) {
    List<Employee> employees = [];
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    String text = "";
    return Scaffold(
      body: Column(children: <Widget>[
        Stack(
          // fit: StackFit.passthrough,
          //  fit: StackFit.loose,
          alignment: Alignment.center,
          children: <Widget>[
            CustomPaint(
              size: Size(double.infinity, height * .50),
              painter: AnalyticsPainter(),
            ),
            Positioned(
              left: width * .03,
              top: height * .03,
              child: Text("Analytics",
                  style: GoogleFonts.roboto(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 28,
                      letterSpacing: 1)),
            ),
            Positioned(
              top: height * .12,
              child: SizedBox(
                child: WalletCard(
                  color: Theme.of(context).primaryColor,
                  price: "\$1660",
                  title: "CREDIT CARD",
                  height: height * .12,
                ),
                height: height * .30,
                width: width * .84,
              ),
            ),
          ],
        ),
        employees.length == 0
            ? FloatingActionButton(
                isExtended: true,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
                child: Text(
                  "get data",
                  style: TextStyle(fontSize: 25),
                ),
                backgroundColor: Theme.of(context).primaryColor,
                onPressed: () => setState(() async {
                      EmployeeData employeeData = EmployeeData();
                      employees = await employeeData.getData();

                      text = employees[1].employee_name;
                    })
                //   Future<List<Employee>> employees;

                )
            : ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: employees.length,
                itemBuilder: (BuildContext context, int index) {
                  print(employees[0].employee_age);
                  return Text(employees[index].employee_name);
                },
                shrinkWrap: true,
                // scrollDirection: Axis.vertical,
              ),

        Text(text),
/*
        Card(
          child: Text(
            text,
            style: TextStyle(
              fontSize: 40,
            ),
          ),
        )*/
        // ListView.builder(
        //   itemCount: 4,
        //   itemBuilder: (BuildContext context, int index) {
        //     print(employees[index].bill);
        //     return HistoryTile(employees[index]);
        //   },
        //   shrinkWrap: true,
        // )
      ]),
    );
  }
}
/*
Widget cardbuilder(BuildContext context, int index) {
  Employee emolyee;
  return Container(
      margin: EdgeInsets.all(20.0),
      child: FutureBuilder<Employee>(
          future: getData(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Text(snapshot.data.employee_name);
            } else if (snapshot.hasError) {
              return Text(snapshot.hasError.toString());
            }
            return Center(child: CircularProgressIndicator());
          }));
}
*/
