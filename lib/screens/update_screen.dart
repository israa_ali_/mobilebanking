import 'package:flutter/material.dart';
import 'package:mobile_banking/Models/employee.dart';
import 'package:mobile_banking/utils/get_data.dart';
import 'package:mobile_banking/widgets/custome_button.dart';
import 'package:mobile_banking/widgets/label.dart';
import 'package:mobile_banking/widgets/text_field.dart';

class UpdateScreen extends StatelessWidget {
  const UpdateScreen({this.employee});
  final Employee employee;
  @override
  Widget build(BuildContext context) {
    TextEditingController name =
        TextEditingController(text: employee.employee_name);
    TextEditingController age =
        TextEditingController(text: employee.employee_age);
    TextEditingController salary =
        TextEditingController(text: employee.employee_salary);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Text(
                "Edit Employee Data",
                style: TextStyle(
                    fontSize: 35,
                    color: Theme.of(context).primaryColor,
                    fontWeight: FontWeight.bold),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 25, right: 25, top: 20, bottom: 10),
                child: TextField(
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(Radius.circular(16)),
                      borderSide: BorderSide(width: 1, color: Colors.grey),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(Radius.circular(16)),
                      borderSide: BorderSide(
                          width: 1, color: Theme.of(context).primaryColor),
                    ),
                  ),
                  controller: name,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 25, right: 25, top: 10, bottom: 10),
                child: TextField(
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(Radius.circular(16)),
                      borderSide: BorderSide(width: 1, color: Colors.grey),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(Radius.circular(16)),
                      borderSide: BorderSide(
                          width: 1, color: Theme.of(context).primaryColor),
                    ),
                  ),
                  controller: age,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 25, right: 25, top: 10, bottom: 10),
                child: TextField(
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(Radius.circular(16)),
                      borderSide: BorderSide(width: 1, color: Colors.grey),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(Radius.circular(16)),
                      borderSide: BorderSide(
                          width: 1, color: Theme.of(context).primaryColor),
                    ),
                  ),
                  controller: salary,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 190, left: 15, right: 15),
                child: Button(
                  onPressed: () {
                    EmployeeData employeeData = EmployeeData();
                    employeeData.updateEmployee(employee, employee.employee_id);
                  },
                  title: "Save",
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
