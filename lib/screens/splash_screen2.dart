import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:mobile_banking/screens/home_screen.dart';
import 'package:mobile_banking/screens/sign_up_screen.dart';
import 'package:mobile_banking/widgets/splash.dart';

class SplashScreens extends StatefulWidget {
  SplashScreens();

  @override
  _SplashScreensState createState() => _SplashScreensState();
}

class _SplashScreensState extends State<SplashScreens> {
  final introKey = GlobalKey<IntroductionScreenState>();

  void _onIntroEnd(context) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => SignUp()),
    );
  }

  Widget _buildImage(String assetName) {
    return Align(
      child: Image.asset('assets/$assetName.png', width: 350.0),
      alignment: Alignment.bottomCenter,
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> screens = [
      Splash(
        imageName: 'assets/splash1.png',
        subtitle: "Mobile banking",
        text:
            "Mobile banking is a service provided by a bank or other financial institution that allows its customers to conduct",
      ),
      Splash(
        imageName: 'assets/splash2.png',
        subtitle: "Mobile banking",
        text:
            "Mobile banking is a service provided by a bank or other financial institution that allows its customers to conduct",
      ),
      Splash(
        imageName: 'assets/splash1.png',
        subtitle: "Mobile banking",
        text:
            "Mobile banking is a service provided by a bank or other financial institution that allows its customers to conduct",
      ),
    ];
    const bodyStyle = TextStyle(fontSize: 19.0, color: Colors.white);
    const pageDecoration = const PageDecoration(
      titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
      bodyTextStyle: bodyStyle,
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: Color(0xff24346C),
      imagePadding: EdgeInsets.zero,
    );

    return IntroductionScreen(
      key: introKey,
      pages: [
        PageViewModel(
          title: "Mobile banking",
          body:
              "Mobile banking is a service provided by a bank or other financial institution that allows its customers to conduct",
          image: _buildImage('splash1'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Easy to booking",
          body:
              "If you’ve been using our credit cards just to fill your tank and buy groceries, you might be passing up a host of travel",
          image: _buildImage('splash2'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Easy to shop",
          body:
              "Making it as easy as possible for your customers to pay is essential for increasing conversions and sales.",
          image: _buildImage('splash3'),
          decoration: pageDecoration,
        ),
      ],
      onDone: () => _onIntroEnd(context),
      //onSkip: () => _onIntroEnd(context), // You can override onSkip callback
      showSkipButton: true,
      skipFlex: 0,
      nextFlex: 0,
      skip: const Text(
        'Skip',
        style: TextStyle(color: Colors.white),
      ),
      next: const Icon(
        Icons.arrow_forward,
        color: Colors.white,
      ),
      done: const Text('Done',
          style: TextStyle(fontWeight: FontWeight.w600, color: Colors.white)),
      dotsDecorator: const DotsDecorator(
        size: Size(10.0, 10.0),
        color: Color(0xFFBDBDBD),
        activeSize: Size(22.0, 10.0),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
      ),
    );
  }
}
