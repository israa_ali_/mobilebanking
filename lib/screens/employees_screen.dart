import 'package:flutter/material.dart';
import 'package:mobile_banking/Models/employee.dart';
import 'package:mobile_banking/utils/get_data.dart';
import 'package:mobile_banking/widgets/employee_card.dart';
import 'package:mobile_banking/widgets/employee_page.dart';
import 'package:mobile_banking/widgets/label.dart';
import 'package:mobile_banking/widgets/sheet.dart';

class EmployeesScreen extends StatefulWidget {
  const EmployeesScreen();

  @override
  _EmployeesScreenState createState() => _EmployeesScreenState();
}

class _EmployeesScreenState extends State<EmployeesScreen> {
  List<Employee> employees = [];
  @override
  void initState() {
    super.initState();
    getData();
  }

  Future<void> deleteEmployee(String id) async {
    employees.removeWhere((element) => element.employee_id == id);
    EmployeeData employeeData = EmployeeData();
    List<Employee> employeesList = await employeeData.getData();

    setState(() {
      employees = employeesList;
    });
  }

  Future<void> getData() async {
    EmployeeData employeeData = EmployeeData();
    List<Employee> employeesList = await employeeData.getData();

    setState(() {
      employees = employeesList;
    });
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 10, top: 50),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[Label(label: "Name"), Label(label: "Salary")],
          ),
        ),
        employees.length == 0
            ? Expanded(child: Center(child: CircularProgressIndicator()))
            : Expanded(
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: employees.length,
                  itemBuilder: (BuildContext context, int index) {
                    print(employees[0].employee_age);
                    return EmployeeCard(employees[index],
                        clickEvent: () => Navigator.of(context).push(
                              MaterialPageRoute(builder: (_) {
                                return EmployeePage(employees[index]);
                              }),
                            ).then((value) {
                              if (value) {
                                deleteEmployee(employees[index].employee_id);
                              }
                            }));
                  },
                  shrinkWrap: true,
                ),
              ),
        Align(
          alignment: Alignment.bottomRight,
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: FloatingActionButton(
              tooltip: "Add new employee",
              isExtended: true,
              onPressed: () => showModalBottomSheet(
                context: context,
                isScrollControlled: true,
                useRootNavigator: true,
                builder: (BuildContext context) => Sheet(),
              ),
              child: Icon(
                Icons.person_add,
                color: Colors.white,
                size: 40,
              ),
              backgroundColor: Theme.of(context).primaryColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(45),
              ),
            ),
          ),
        )
      ],
    );
  }
}
