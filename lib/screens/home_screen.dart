import 'package:flutter/material.dart';
import 'package:mobile_banking/Models/employee.dart';
import 'package:mobile_banking/screens/analytics_screen.dart';
import 'package:mobile_banking/screens/employees_screen.dart';
import 'package:mobile_banking/screens/history_screen.dart';
import 'package:mobile_banking/screens/profile_screen2.dart';
import 'package:mobile_banking/screens/wallet_screen.dart';
import 'package:mobile_banking/utils/get_data.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentIndex = 0;

  final List<Widget> _children = [
    WalletScreen(),
    HistroyScreen(),
    EmployeesScreen(),
    ProfileScreen(),
  ];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        elevation: 10,
        unselectedItemColor: Colors.black54,
        selectedItemColor: Theme.of(context).primaryColor,
        iconSize: 35,
        showUnselectedLabels: true,
        type: BottomNavigationBarType.fixed,
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.account_balance_wallet),
            title: Text(""),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.folder_open),
            title: Text(""),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.pie_chart),
            title: Text(""),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text(""),
          ),
        ],
      ),
    );
  }
}
