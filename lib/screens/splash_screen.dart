import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:mobile_banking/widgets/splash.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final _totalDots = 3;
  double _currentPosition = 0.0;
  PageController _controller = PageController(
    initialPage: 0,
  );
  double _validPosition(double position) {
    if (position >= _totalDots) return 0;
    if (position < 0) return _totalDots - 1.0;
    return position;
  }

  void _updatePosition(double position) {
    setState(() => _currentPosition = _validPosition(position));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Widget _buildRow(List<Widget> widgets) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: widgets,
      ),
    );
  }

  String getCurrentPositionPretty() {
    return (_currentPosition + 1.0).toStringAsPrecision(2);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        elevation: 0,
        actions: <Widget>[
          RaisedButton(
            color: Theme.of(context).primaryColor,
            onPressed: () {},
            child: Text(
              "skip",
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Flexible(
            fit: FlexFit.tight,
            child: PageView(
              scrollDirection: Axis.horizontal,
              controller: _controller,
              children: [
                Splash(
                  imageName: 'assets/splash1.png',
                  subtitle: "Mobile banking",
                  text:
                      "Mobile banking is a service provided by a bank or other financial institution that allows its customers to conduct",
                ),
                Splash(
                  imageName: 'assets/splash2.png',
                  subtitle: "Mobile banking",
                  text:
                      "Mobile banking is a service provided by a bank or other financial institution that allows its customers to conduct",
                ),
                Splash(
                  imageName: 'assets/splash1.png',
                  subtitle: "Mobile banking",
                  text:
                      "Mobile banking is a service provided by a bank or other financial institution that allows its customers to conduct",
                ),
              ],
            ),
          ),
          _buildRow([
            DotsIndicator(
              dotsCount: _totalDots,
              position: _currentPosition,
              decorator: DotsDecorator(
                size: const Size.square(9.0),
                activeSize: const Size(8.0, 8.0),
                activeShape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0)),
              ),
            ),
          ]),
        ],
      ),
    );
  }
}
