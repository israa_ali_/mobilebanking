import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobile_banking/Models/history_item.dart';
import 'package:mobile_banking/history_items_list.dart';
import 'package:mobile_banking/widgets/history_tile.dart';
import 'package:mobile_banking/widgets/operation_button.dart';
import 'package:mobile_banking/widgets/snapping_sheet.dart';
import 'package:mobile_banking/widgets/wallet_card.dart';
import 'package:mobile_banking/widgets/wallet_painter.dart';

class WalletScreen extends StatefulWidget {
  const WalletScreen();

  @override
  _WalletScreenState createState() => _WalletScreenState();
}

class _WalletScreenState extends State<WalletScreen> {
  @override
  Widget build(BuildContext context) {
    List<HistoryItem> historyItems = HistoryItemsList().historyItems;

    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return Stack(
      children: [
        Column(
          children: <Widget>[
            Stack(
              // fit: StackFit.passthrough,
              fit: StackFit.loose,
              alignment: Alignment.center,
              children: <Widget>[
                CustomPaint(
                  size: Size(double.infinity, height * .50),
                  painter: WalletPainter(),
                ),
                Positioned(
                  left: width * .03,
                  top: height * .03,
                  child: Text("Wallet",
                      style: GoogleFonts.roboto(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 28,
                          letterSpacing: 1)),
                ),

                Positioned(
                  top: height * .07,
                  child: SizedBox(
                    child: WalletCard(color: Colors.grey),
                    height: height * .25,
                    width: width * .74,
                  ),
                ),

                Positioned(
                  top: height * .08,
                  child: SizedBox(
                    child: WalletCard(color: Colors.black),
                    height: height * .25,
                    width: width * .78,
                  ),
                ),

                Positioned(
                  top: height * .1,
                  child: SizedBox(
                    child: WalletCard(color: const Color(0xffF6CAC2)),
                    height: height * .25,
                    width: width * .80,
                  ),
                ),

                Positioned(
                  top: height * .12,
                  child: SizedBox(
                    child: WalletCard(
                      color: Theme.of(context).primaryColor,
                      price: "\$1660",
                      title: "CREDIT CARD",
                      height: height * .12,
                    ),
                    height: height * .30,
                    width: width * .84,
                  ),
                ),
                //  WalletCard(),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(left: width * .06, right: width * .06),
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Popular operations",
                      style: GoogleFonts.roboto(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).primaryColor,
                          fontSize: 22),
                    ),
                  ),
                  SizedBox(height: height * .02),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      OperationButton(
                        icon: Icons.home,
                        color: Theme.of(context).primaryColor,
                        height: height,
                        width: width,
                        title: "All",
                      ),
                      OperationButton(
                        icon: Icons.favorite,
                        color: Colors.red,
                        height: height,
                        width: width,
                        title: "Health",
                      ),
                      OperationButton(
                        icon: Icons.airplanemode_active,
                        color: Theme.of(context).primaryColor,
                        height: height,
                        width: width,
                        title: "Travel",
                      ),
                      OperationButton(
                        icon: Icons.card_travel,
                        color: Colors.red,
                        height: height,
                        width: width,
                        title: "Food",
                      ),
                    ],
                  ),

                  /*  BottomSheet(
                  
                    enableDrag: true,
                    builder: (BuildContext context) => SizedBox(
                      width: width * 8,
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              height: height * .015,
                              width: width * .25,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: Colors.black12),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "History",
                                  style: GoogleFonts.roboto(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w800,
                                    color: Colors.black,
                                  ),
                                ),
                                Icon(Icons.search)
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    onClosing: () {},
                  )*/
                ],
              ),
            )
          ],
        ),
        DraggableScrollableSheet(
          maxChildSize: 0.8,
          initialChildSize: 0.25,
          builder: (context, ScrollController scrollController) =>
              SingleChildScrollView(
            controller: scrollController,
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20), color: Colors.white),
              height: height,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: height * .015,
                      width: width * .25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.black12),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "History",
                          style: GoogleFonts.roboto(
                            fontSize: 20,
                            fontWeight: FontWeight.w800,
                            color: Colors.black,
                          ),
                        ),
                        Icon(Icons.search)
                      ],
                    ),
                    ListView.builder(
                      itemCount: historyItems.length,
                      itemBuilder: (BuildContext context, int index) {
                        print(historyItems[3].bill);
                        return HistoryTile(historyItems[index]);
                      },
                      shrinkWrap: true,
                      // scrollDirection: Axis.vertical,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
