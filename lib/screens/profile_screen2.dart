import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobile_banking/widgets/profile_options_tile.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen();

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    GetStorage getStorage = GetStorage();
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Profile",
                  style: GoogleFonts.roboto(
                      color: Colors.black,
                      fontSize: 30,
                      fontWeight: FontWeight.bold)),
              IconButton(
                  icon: Icon(
                    Icons.more_vert,
                    size: 30,
                    color: Colors.black,
                  ),
                  onPressed: () {})
            ],
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Image.asset('assets/Frame.png'),
                Positioned(
                    top: height * .1,
                    left: width * .17,
                    child: FloatingActionButton(
                      onPressed: () {},
                      mini: true,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16),
                      ),
                      backgroundColor: Colors.white,
                      child: Icon(
                        Icons.edit,
                        color: Color(0xff24346C),
                      ),
                    ))
              ],
            ),
            Text(
              "Kristin Waston",
              style: GoogleFonts.roboto(
                  color: Colors.black,
                  fontSize: 25,
                  fontWeight: FontWeight.bold),
            ),
            Text(
              "333 555 666",
              style: GoogleFonts.roboto(
                  color: Colors.black,
                  fontSize: 23,
                  fontWeight: FontWeight.w600),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Container(
                width: width,
                height: height * .50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: Colors.grey[100]),
                child: Column(
                  children: <Widget>[
                    ProfileOptionTile(
                      icon: Icons.web,
                      color: Color(0xffF9DDDF),
                      text: "Order Extra Card",
                      iconColor: Color(0xffF56962),
                      onPressed: () {},
                    ),
                    ProfileOptionTile(
                      icon: Icons.person,
                      color: Color(0xffCFD2E1),
                      text: "Personal Details",
                      iconColor: Color(0xff24346C),
                      onPressed: () {},
                    ),
                    ProfileOptionTile(
                      icon: Icons.lock,
                      color: Color(0xffF7D7E7),
                      text: "Privacy & Security",
                      iconColor: Color(0xffEA4C89),
                      onPressed: () {},
                    ),
                    ProfileOptionTile(
                      icon: Icons.info,
                      color: Color(0xffFBF0CB),
                      text: "Legal",
                      iconColor: Color(0xffFFC600),
                      onPressed: () {},
                    ),
                    ProfileOptionTile(
                        icon: Icons.exit_to_app,
                        color: Color(0xffFBD2E0),
                        text: "Log out",
                        iconColor: Color(0xffFF3366),
                        onPressed: () {
                          getStorage.erase();
                        }),
                  ],
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
