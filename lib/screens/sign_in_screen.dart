import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobile_banking/screens/sign_up_screen.dart';
import 'package:mobile_banking/widgets/custome_button.dart';
import 'package:mobile_banking/widgets/label.dart';
import 'package:mobile_banking/widgets/signin_painter.dart';
import 'package:mobile_banking/widgets/text_field.dart';

class SignIn extends StatelessWidget {
  const SignIn();

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
                alignment: Alignment.centerLeft,
                fit: StackFit.loose,
                children: [
                  CustomPaint(
                    size: Size(double.infinity, height * .38),
                    painter: SignInPainter(),
                  ),
                  Positioned(
                    top: height * .12,
                    left: width * .065,
                    child: Text(
                      "Sign In",
                      style: GoogleFonts.roboto(
                        fontSize: 30,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Positioned(
                      top: height * .18,
                      left: width * .065,
                      child: Row(
                        children: <Widget>[
                          Text(
                            "Don't have an account? ",
                            style: TextStyle(fontSize: 20, color: Colors.white),
                          ),
                          RaisedButton(
                            onPressed: () {},
                            color: const Color(0xffF56962),
                            elevation: 0,
                            child: Text(
                              "Sign up!",
                              style: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontSize: 18),
                            ),
                          )
                        ],
                      ))
                ]),
            Label(label: "Email"),
            InputField(placeHolder: "username@gmail.com"),
            Label(label: "Password"),
            InputField(placeHolder: "********"),
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Row(
                children: <Widget>[
                  Checkbox(value: false, onChanged: null),
                  Text("Forgot your password ? ")
                ],
              ),
            ),
            SizedBox(height: height * .15),
            Button(title: "Sign In"),
          ],
        ),
      ),
    );
  }
}
