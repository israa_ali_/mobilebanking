import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobile_banking/widgets/custome_button.dart';
import 'package:mobile_banking/widgets/painter.dart';
import 'package:mobile_banking/widgets/signup_form.dart';
import 'package:mobile_banking/widgets/text_field.dart';

class SignUp extends StatelessWidget {
  const SignUp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              fit: StackFit.loose,
              alignment: Alignment.centerLeft,
              children: <Widget>[
                CustomPaint(
                  size: Size(double.infinity, height * .38),
                  painter: Painter(),
                ),
                Positioned(
                  top: height * .12,
                  left: width * .065,
                  child: Text(
                    "Create Account",
                    style: TextStyle(
                      fontSize: 28,
                      color: Colors.white,
                    ),
                  ),
                ),
                Positioned(
                    top: height * .18,
                    left: width * .065,
                    child: Row(
                      children: <Widget>[
                        Text(
                          "Already have an account? ",
                          style: TextStyle(fontSize: 18, color: Colors.white),
                        ),
                        RaisedButton(
                          onPressed: () {},
                          color: const Color(0xffF6CAC2),
                          elevation: 0,
                          child: Text(
                            "Sign in!",
                            style: TextStyle(color: Colors.red, fontSize: 18),
                          ),
                        )
                      ],
                    )),
              ],
            ),
            SignUpForm(),
          ],
        ),
      ),
    );
  }
}
