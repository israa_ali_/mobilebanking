class Employee {
  Employee({
    this.employee_name,
    this.employee_salary,
    this.employee_age,
    this.employee_id = null,
  });

  String employee_name;
  String employee_salary;
  String employee_age;
  String employee_id;

  Employee.fromJson(Map<String, dynamic> json)
      : employee_name = json['employee_name'],
        employee_salary = json['employee_salary'],
        employee_age = json['employee_age'],
        employee_id = json['employee_id'];
}
