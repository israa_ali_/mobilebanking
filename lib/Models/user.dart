import 'dart:convert';

class User {
  String userName;
  String phone;
  String password;
  String email;

  set setUserName(String userName) => this.userName = userName;

  set setEmail(String email) => this.email = email;

  set setPhone(String phone) => this.phone = phone;

  set setPassword(String password) => this.password = password;
  User({
    this.userName,
    this.email,
    this.phone,
    this.password,
  });

  Map<String, dynamic> toMap() {
    return {
      'userName': userName,
      'email': email,
      'phone': phone,
      'password': password,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return User(
      userName: map['userName'],
      email: map['email'],
      phone: map['phone'],
      password: map['password'],
    );
  }

  String toJson() => json.encode(toMap());

  factory User.fromJson(String source) => User.fromMap(json.decode(source));
}
