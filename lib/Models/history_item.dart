import 'package:flutter/cupertino.dart';

class HistoryItem {
  const HistoryItem(
      {this.name, this.bill, this.icon, this.iconColor, this.date});
  final IconData icon;
  final Color iconColor;
  final String name;
  final String date;
  final String bill;
}
