import 'Models/history_item.dart';
import 'package:flutter/material.dart';

class HistoryItemsList {
  List<HistoryItem> get historyItems => [
        HistoryItem(
            name: "Dribbble",
            icon: Icons.add_circle_outline,
            iconColor: Colors.pink[400],
            date: "13:14, 09 Aug 2020",
            bill: "-\$60.00"),
        HistoryItem(
            name: "InVesion",
            icon: Icons.add_circle_outline,
            iconColor: Colors.pink[600],
            date: "13:14, 09 Aug 2020",
            bill: "-\$40.00"),
        HistoryItem(
            name: "McDonald's",
            icon: Icons.add_circle_outline,
            iconColor: Colors.yellow,
            date: "13:14, 09 Aug 2020",
            bill: "-\$60.00"),
        HistoryItem(
            name: "Dribbble",
            icon: Icons.add_circle_outline,
            iconColor: Colors.pink[400],
            date: "13:14, 09 Aug 2020",
            bill: "-\$60.00"),
      ];
}
