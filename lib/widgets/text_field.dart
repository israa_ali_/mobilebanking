import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InputField extends StatelessWidget {
  const InputField({this.placeHolder, this.controller, this.validator});
  final String placeHolder;
  final TextEditingController controller;
  final String Function(String) validator;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 25, right: 25, top: 10, bottom: 10),
      child: TextFormField(
        controller: controller,
        validator: validator
        /*(value) {
          if (value.isEmpty) {
            return "Please Enter some text";
          }
          return null;
        },*/
        ,
        decoration: InputDecoration(
          hintText: placeHolder,
          enabledBorder: OutlineInputBorder(
            borderRadius: const BorderRadius.all(Radius.circular(16)),
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: const BorderRadius.all(Radius.circular(16)),
            borderSide:
                BorderSide(width: 1, color: Theme.of(context).primaryColor),
          ),
        ),
      ),
    );
  }
}
