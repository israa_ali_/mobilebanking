import 'package:flutter/cupertino.dart';
import 'package:mobile_banking/screens/sign_in_screen.dart';

class SignInPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final width = size.width;
    final height = size.height;

    final paint = Paint()
      ..color = const Color(0xffF56962)
      ..strokeWidth = 4
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill;
    final bigCircle = Offset(width * .80, height * .2);
    canvas.drawCircle(bigCircle, width * .45, paint);

    final paint2 = Paint()
      ..color = const Color(0xffF6CAC2)
      ..strokeWidth = 4
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill;
    final smallCircle = Offset(width * .2, height * .3);
    canvas.drawCircle(smallCircle, width * .33, paint2);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
