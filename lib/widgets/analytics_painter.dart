import 'package:flutter/cupertino.dart';
import 'package:mobile_banking/widgets/painter.dart';

class AnalyticsPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final width = size.width;
    final height = size.height;

    final paint = Paint()
      ..color = const Color(0xff24346C)
      ..strokeWidth = 4
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill;

    final path = Path()
      ..lineTo(0, height * .6)
      ..quadraticBezierTo(
        width / 2,
        height,
        width,
        height * .6,
      )
      ..lineTo(width, 0)
      ..close();

    canvas.drawPath(path, paint);
    final paint2 = Paint()
      ..color = const Color(0xffF56962)
      ..strokeWidth = 4
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill;

    final Rect myRect =
        const Offset(150, -150) & Size(width * .8, height * .70);
    canvas.drawOval(myRect, paint2);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
