import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ProfileOptionTile extends StatelessWidget {
  const ProfileOptionTile(
      {this.icon, this.text, this.color, this.iconColor, this.onPressed});
  final IconData icon;
  final String text;
  final Color color;
  final Color iconColor;
  final void Function() onPressed;

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return Padding(
      padding: const EdgeInsets.only(top: 6),
      child: RaisedButton(
        color: Colors.grey[100],
        elevation: 0,
        onPressed: onPressed,
        child: ListTile(
          leading: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(18),
                color: color,
              ),
              height: height * .08,
              width: width * .13,
              child: Icon(
                icon,
                size: 30,
                color: iconColor,
              )),
          title: Text(
            text,
            style: GoogleFonts.roboto(
                color: Colors.black, fontSize: 18, fontWeight: FontWeight.w700),
          ),
          trailing: Icon(
            Icons.chevron_right,
          ),
        ),
      ),
    );
  }
}
