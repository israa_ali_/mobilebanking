import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Label extends StatelessWidget {
  const Label({@required this.label});
  final String label;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 25),
      child: Align(
        alignment: Alignment.topLeft,
        child: Text(label,
            style: GoogleFonts.roboto(
                color: Theme.of(context).primaryColor,
                fontWeight: FontWeight.w700,
                fontSize: 20)),
      ),
    );
  }
}
