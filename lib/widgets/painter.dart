import 'package:flutter/cupertino.dart';

class Painter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final width = size.width;
    final height = size.height;

///////////////////////////////////// Blue Circle ///////////////////////////////////////////////////

    final paint = Paint()
      ..color = const Color(0xff24346C)
      ..strokeWidth = 4
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill;
    final blueCircle = Offset(width * .3, height * .2);
    canvas.drawCircle(blueCircle, width * .35, paint);

///////////////////////////////////// Orange Circle ///////////////////////////////////////////////////
    final paint2 = Paint()
      ..color = const Color(0xffF56962)
      ..strokeWidth = 4
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill;

    final Rect myRect =
        const Offset(200, -120) & Size(width * .7, height * .80);
    canvas.drawOval(myRect, paint2);

///////////////////////////////////// Baby pink Circle ///////////////////////////////////////////////////

    final paint3 = Paint()
      ..color = const Color(0xffF6CAC2)
      ..strokeWidth = 4
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill;

    final circle = Offset(width * .80, height * .52);
    canvas.drawCircle(circle, width * .27, paint3);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
