import 'package:flutter/cupertino.dart';
import 'package:mobile_banking/widgets/painter.dart';

class WalletPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final width = size.width;
    final height = size.height;

    final paint = Paint()
      ..color = const Color(0xff24346C)
      ..strokeWidth = 4
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill;

    final path = Path()
      ..lineTo(0, height * .6)
      ..quadraticBezierTo(
        width / 2,
        height,
        width,
        height * .6,
      )
      ..lineTo(width, 0)
      ..close();

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
