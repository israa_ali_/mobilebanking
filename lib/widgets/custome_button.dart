import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  const Button({this.title, this.onPressed});
  final String title;
  final void Function() onPressed;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width * .90,
      height: MediaQuery.of(context).size.height * .08,
      child: RaisedButton(
        padding: EdgeInsetsDirectional.fromSTEB(25, 5, 25, 5),
        color: Theme.of(context).primaryColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        onPressed: onPressed,
        child: Padding(
          padding: const EdgeInsetsDirectional.fromSTEB(20, 8, 20, 10),
          child: Text(title,
              style: TextStyle(
                  fontSize: 24,
                  color: Colors.white,
                  fontWeight: FontWeight.bold)),
        ),
      ),
    );
  }
}
