import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Paragraph extends StatelessWidget {
  const Paragraph(this.text);
  final String text;
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        constraints: BoxConstraints(
          maxWidth: MediaQuery.of(context).size.width * .7,
          minHeight: MediaQuery.of(context).size.height * .5,
        ),
        child: Text(text,
            style: GoogleFonts.roboto(
              color: Colors.white,
              fontSize: 18,
            )),
      ),
    );
  }
}
