import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobile_banking/widgets/card_painter.dart';

class WalletCard extends StatelessWidget {
  const WalletCard({this.color, this.title, this.price, this.height});
  final Color color;
  final String title;
  final String price;
  final double height;
  @override
  Widget build(BuildContext context) {
    return Card(
      color: color,
      margin: EdgeInsets.all(8),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
      ),
      elevation: 5,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              textDirection: TextDirection.ltr,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                title != null
                    ? Text(
                        title,
                        style: GoogleFonts.roboto(
                          fontSize: 20,
                          color: Colors.white,
                        ),
                      )
                    : Text(""),
                price != null
                    ? Text(
                        price,
                        style: GoogleFonts.roboto(
                          fontSize: 20,
                          color: Colors.white,
                        ),
                      )
                    : Text(""),
              ],
            ),
            height != null
                ? Expanded(
                    child: CustomPaint(
                      size: Size(double.infinity, height),
                      painter: CardPainter(),
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
