import 'package:flutter/material.dart';
import 'package:mobile_banking/Models/history_item.dart';

class HistoryTile extends StatelessWidget {
  const HistoryTile(this.historyItem);
  final HistoryItem historyItem;
  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.all(8),
      // isThreeLine: true,
      leading: Container(
        child: Icon(
          historyItem.icon,
          size: 24,
        ),
        decoration: BoxDecoration(
          color: historyItem.iconColor,
          borderRadius: BorderRadius.circular(18),
        ),
      ),
      title: Text(historyItem.name),
      subtitle: Text(historyItem.date),
      trailing: Text(historyItem.bill),
    );
  }
}
