import 'package:flutter/material.dart';

class Subtitle extends StatelessWidget {
  const Subtitle(this.subtitle);
  final String subtitle;

  @override
  Widget build(BuildContext context) {
    return Text(
      subtitle,
      style: TextStyle(
          color: Colors.white, fontSize: 30, fontFamily: 'Roboto-Regular'),
    );
  }
}
