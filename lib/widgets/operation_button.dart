import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class OperationButton extends StatelessWidget {
  const OperationButton(
      {this.icon, this.color, this.height, this.width, this.title});
  final IconData icon;
  final Color color;
  final double height;
  final double width;
  final String title;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: height * .10,
          width: width * .18,
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            padding: EdgeInsets.all(8),
            color: Colors.white,
            elevation: 4,
            onPressed: () {},
            child: Icon(
              icon,
              color: color,
              size: width * .1,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            title,
            style: GoogleFonts.roboto(
                color: Theme.of(context).primaryColor,
                fontSize: 18,
                fontWeight: FontWeight.w800),
          ),
        )
      ],
    );
  }
}
