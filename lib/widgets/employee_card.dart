import 'package:flutter/material.dart';
import 'package:mobile_banking/Models/employee.dart';
import 'package:mobile_banking/widgets/employee_page.dart';

class EmployeeCard extends StatelessWidget {
  const EmployeeCard(this.employee, {this.clickEvent});
  final Employee employee;
  final Function clickEvent;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      focusColor: Theme.of(context).primaryColor,
      onTap: clickEvent,
      child: ListTile(
        contentPadding: EdgeInsets.all(20),
        enabled: true,
        title: Text(employee.employee_name),
        subtitle: Text(employee.employee_age),
        trailing: Text(employee.employee_salary),
      ),
    );
  }
}
