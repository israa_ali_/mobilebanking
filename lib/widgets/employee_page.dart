import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mobile_banking/Models/employee.dart';
import 'package:mobile_banking/screens/update_screen.dart';
import 'package:mobile_banking/utils/get_data.dart';
import 'package:mobile_banking/widgets/label.dart';
import 'package:path/path.dart';

class EmployeePage extends StatefulWidget {
  const EmployeePage(this.employee);
  final Employee employee;

  @override
  _EmployeePageState createState() => _EmployeePageState();
}

class _EmployeePageState extends State<EmployeePage> {
  EmployeeData employeeData;
  File _imageFile;
  final picker = ImagePicker();
  File tmpFile;
//
  Future getImage() async {
    var image = await picker.getImage(source: ImageSource.gallery);

    tmpFile = File(image.path);

    final Directory directory = await getApplicationDocumentsDirectory();

    final String fileName = basename(image.path); // Filename without extension
    final String fileExtension = extension(image.path); // e.g. '.jpg'
    final String path = directory.path;
    tmpFile = await tmpFile.copy('$path/$fileName');

    setState(() {
      print(tmpFile);

      _imageFile = tmpFile;
      //  employeeData.uploadPhoto(_imageFile.toString(), tmpFile.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    bool isDeleted = false;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: getImage,
        tooltip: 'Pick Image',
        child: Icon(Icons.add_a_photo),
      ),
      body: Center(
          child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(15),
            child: Container(
              height: 120,
              width: 120,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(55),
                color: Theme.of(context).primaryColor,
              ),
              child: _imageFile == null
                  ? Text('No image selected.')
                  : Image.file(_imageFile),
            ),
          ),
          Label(label: widget.employee.employee_name),
          Label(label: widget.employee.employee_age),
          Label(label: widget.employee.employee_salary),
          RaisedButton(
            onPressed: () => Navigator.of(context).push(
              MaterialPageRoute(builder: (_) {
                return UpdateScreen(
                  employee: widget.employee,
                );
              }),
            ),
            elevation: 0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20), side: BorderSide.none),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(
                "update account",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
            ),
            color: Theme.of(context).primaryColor,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: RaisedButton(
              onPressed: () {
                EmployeeData employeeData = EmployeeData();
                employeeData.deleteEmployee(widget.employee.employee_id);
                setState(() {
                  isDeleted = true;
                });

                Navigator.pop(
                  context,
                  isDeleted,
                );
              },
              elevation: 0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                  side: BorderSide.none),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  "Delete account",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
              color: Colors.red,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: RaisedButton(
              onPressed: () {
                EmployeeData employeeData = EmployeeData();
                employeeData.uploadPhoto(_imageFile.toString(), tmpFile.path);
                setState(() {
                  isDeleted = true;
                });
              },
              elevation: 0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                  side: BorderSide.none),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  "send",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
              color: Colors.green,
            ),
          ),
        ],
      )),
    );
  }
}
