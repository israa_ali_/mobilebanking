import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile_banking/Models/user.dart';
import 'package:mobile_banking/screens/home_screen.dart';
import 'package:mobile_banking/utils/validator.dart';
import 'package:mobile_banking/widgets/custome_button.dart';
import 'package:mobile_banking/widgets/label.dart';
import 'package:mobile_banking/widgets/text_field.dart';

class SignUpForm extends StatefulWidget {
  SignUpForm();

  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();
  final User user = User();
  final getStorage = GetStorage();
  final userName = TextEditingController();
  final email = TextEditingController();
  final phone = TextEditingController();
  final password = TextEditingController();
  final confirmPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Label(label: "User name"),
          InputField(
            controller: userName,
            placeHolder: "Kristin Waston",
            validator: (userName) {
              if (userName.isEmpty) {
                return "Please Enter some text";
              } else {
                if (Validator.validateUserName(userName))
                  return null;
                else
                  return "please enter a valid username";
              }
            },
          ),
          Label(label: "Email"),
          InputField(
            controller: email,
            placeHolder: "username@gmail.com",
            validator: (email) {
              if (email.isEmpty) {
                return "Please Enter some text";
              } else {
                if (Validator.validateEmail(email))
                  return null;
                else
                  return "please enter a valid email";
              }
            },
          ),
          Label(label: "Phone"),
          InputField(
            controller: phone,
            placeHolder: "(408) 612-1120",
            validator: (phone) {
              if (phone.isEmpty) {
                return "Please Enter some text";
              } else {
                if (Validator.validatePhone(phone))
                  return null;
                else
                  return "please enter a valid phone number";
              }
            },
          ),
          Label(label: "Password"),
          InputField(
            controller: password,
            placeHolder: "********",
            validator: (password) {
              if (password.isEmpty) {
                return "Please Enter some text";
              } else {
                if (Validator.validatePassword(password))
                  return null;
                else
                  return "please enter a valid password";
              }
            },
          ),
          Label(label: "Confirm Password"),
          InputField(
            controller: password,
            placeHolder: "********",
            validator: (confirmPassword) {
              if (confirmPassword.isEmpty) {
                return "Please Enter some text";
              } else {
                return null;
              }
              /*else {
                if (confirmPassword != password)
                  return "Password confirmation and Password must match.";
                else
                  return null;
              }*/
            },
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * .05,
          ),
          Button(
            title: "Sign up",
            onPressed: () {
              if (_formKey.currentState.validate()) {
                // If the form is valid, display a snackbar. In the real world,
                // you'd often call a server or save the information in a database.
                user.userName = userName.toString();
                user.email = email.toString();
                user.phone = phone.toString();
                user.password = password.toString();
//user.userName=userName.toString();
                user.toMap();
                getStorage.write("user", user.toMap());
                print(getStorage.read("user"));
                user.setUserName = userName.text.toString();
                user.setEmail = email.text.toString();
                user.setPhone = phone.text.toString();
                user.setPassword = password.text.toString();
                print(user.email);

                getStorage.write("user", user.toMap());
                var us = User.fromMap(getStorage.read("user"));
                print(us);
                Scaffold.of(context)
                    .showSnackBar(SnackBar(content: Text('Processing Data')));
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (_) => HomeScreen()),
                );
              }
            },
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * .05,
          ),
        ],
      ),
    );
  }
}
