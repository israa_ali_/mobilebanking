import 'package:flutter/material.dart';
import 'package:mobile_banking/widgets/paragraph.dart';
import 'package:mobile_banking/widgets/subtitle.dart';

class Splash extends StatelessWidget {
  const Splash({
    @required this.imageName,
    @required this.text,
    @required this.subtitle,
  });
  final String imageName;
  final String text;
  final String subtitle;
  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 4,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: MediaQuery.of(context).size.height * .4,
            width: MediaQuery.of(context).size.width * .8,
            child: Image.asset(imageName),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * .08,
            child: Subtitle(subtitle),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * .4,
            child: Paragraph(text),
          ),
        ],
      ),
    );
  }
}
