import 'package:flutter/material.dart';
import 'package:mobile_banking/Models/employee.dart';
import 'package:mobile_banking/utils/validator.dart';
import 'package:mobile_banking/widgets/custome_button.dart';
import 'package:mobile_banking/widgets/label.dart';
import 'package:mobile_banking/widgets/text_field.dart';
import 'package:mobile_banking/utils/get_data.dart';

class Sheet extends StatefulWidget {
  Sheet({this.employee});
  Employee employee;
  @override
  _SheetState createState() => _SheetState();
}

class _SheetState extends State<Sheet> {
  TextEditingController employeeName = TextEditingController();
  TextEditingController employeeAge = TextEditingController();
  TextEditingController employeeSalary = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.6,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: <Widget>[
                Label(label: "Employee Name"),
                InputField(
                  controller: employeeName,
                  placeHolder: "Adam irish",
                  validator: (employeeName) {
                    return null;
                  },
                ),
                Label(label: "Employee Age"),
                InputField(
                  controller: employeeAge,
                  placeHolder: "45",
                  validator: (employeeAge) {
                    return null;
                  },
                ),
                Label(label: "Employee Salary"),
                InputField(
                  controller: employeeSalary,
                  placeHolder: "1200",
                  validator: (employeeSalary) {
                    return null;
                  },
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
                  child: Button(
                    onPressed: () {
                      Employee employee = Employee(
                          employee_name: employeeName.toString(),
                          employee_age: employeeAge.toString(),
                          employee_salary: employeeSalary.toString());
                      EmployeeData employeeData = EmployeeData();
                      employeeData.addEmployee(employee);
                    },
                    title: "Add",
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
