import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile_banking/Models/user.dart';
import 'package:mobile_banking/screens/home_screen.dart';
import 'package:mobile_banking/screens/sign_in_screen.dart';
import 'package:mobile_banking/screens/sign_up_screen.dart';
import 'package:mobile_banking/screens/splash_screen2.dart';

void main() async {
  await GetStorage.init();

  // print(response.data);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    GetStorage getStorage = GetStorage();
    dynamic user = getStorage.read('user');

    //getStorage.erase();
    print(getStorage.toString());
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.dark.copyWith(statusBarColor: Colors.transparent),
    );
    return MaterialApp(
      //  home: SplashScreens(),
      home: user == null ? SplashScreens() : HomeScreen(),
      theme: ThemeData(
        primaryColor: const Color(0xff24346C),
        // fontFamily: GoogleFonts.roboto(),
      ),
    );
  }
}
