import 'dart:convert';
import 'dart:math';

import 'package:dio/dio.dart';
import 'package:mobile_banking/Models/employee.dart';

class EmployeeData {
  var dio = Dio();
  Employee employee;
  List<Employee> employees;
  Response response = Response();
  Future<List<Employee>> getData() async {
    response =
        await dio.get('http://dummy.restapiexample.com/api/v1/employees');
    if (response.statusCode == 200) {
      print(response.data);
      final data = response.data['data'] as List;
      return List<Employee>.from(data.map((e) => Employee.fromJson(e)));
    } else {
      print('error');
      return null;
    }
  }

  Future<void> addEmployee(Employee employee) async {
    var response =
        await dio.post('http://dummy.restapiexample.com/api/v1/create', data: {
      'employee_name': employee.employee_name,
      'employee_salary': employee.employee_salary,
      'employee_age': employee.employee_age
    });

    print(response);
  }

  Future<void> updateEmployee(Employee employee, String id) async {
    var response = await dio
        .put('http://dummy.restapiexample.com/api/v1/update/$id', data: {
      'employee_name': employee.employee_name,
      'employee_salary': employee.employee_salary,
      'employee_age': employee.employee_age
    });
    print(response);
  }

  Future<void> deleteEmployee(String id) async {
    try {
      var response = await dio.delete(
        'http://dummy.restapiexample.com/api/v1/delete/$id',
      );
    } on DioError catch (e) {
      print(response.request);
      print(e.message);
    }

    print(id);
  }

  Future<void> uploadPhoto(String fileName, String path) async {
    var formData = FormData();

    formData.files.add(MapEntry(
        'files', MultipartFile.fromFileSync(path, filename: fileName)));
    response = await dio.post('https://httpbin.org/post', data: formData);

    print(response);
  }
}
