class Validator {
  static bool validateUserName(String userName) =>
      RegExp(r'^(?=[a-zA-Z0-9._]{8,20}$)(?!.*[_.]{2})[^_.].*[^_.]$')
          .hasMatch(userName);

  static bool validateEmail(String email) =>
      RegExp(r'[\w\d]{4,}@[\w\d]{2,}\.[\w\d]+').hasMatch(email);

  static bool validatePhone(String phone) =>
      RegExp(r'^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$')
          .hasMatch(phone);

  static bool validatePassword(String password) => password.length >= 8;
}
